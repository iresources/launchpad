### 0.0.16
- Default 1s delay for Drakov API Blueprint server

### 0.0.15
- Added "watch" and "serverPort" options to startBlueprintServer command

### 0.0.14
- Missing hashbang from scripts.js

### 0.0.13
- Added scripts.js reference to .bin folder
- Renamed apiDevServer command to startBlueprintServer and added optional folder parameter

### 0.0.12
- No longer exporting packages, only components and helpers.

### 0.0.11
- Moved index.html file. It could possibly be causing issues with parceljs.

### 0.0.10
- Move ./demo/index.html to ./index.html

### 0.0.9
- Export axios library in packages for making network requests

### 0.0.8
- Export connect from react-redux as "connectStore"

### 0.0.7
- Forgot to build before the last release!

### 0.0.6
- Export "Connect" package from react-redux
- Renamed App.js to App.jsx and fixed some linting errors

### 0.0.5
- Un-ignored the ./dist folder. Oops!

### 0.0.4
- Added missing "typescript" and "parcel-bundler" dependencies

### 0.0.3
- Added "parcel-plugin-typescript" that will display Typescript errors in the console

### 0.0.2
- Added "apiDevServer" command that will serve any .apib files in "api_blueprints" file via Drakov library

### 0.0.1
- Basic bundling via parceljs via "watch", "serve" commands
- Added "copyTemplates" command that copies files from './templates' directory to parent. Currently will always overwrite files.
