import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Launchpad from '../dist/index.js'

const { render } = Launchpad.helpers;
const { App } = Launchpad.components;

const reducers = {
    thisUser: (state = { forename: 'Paddington', surname: 'Bear' }, action) => {
        switch (action.type) {
        default:
            return state;
        }
    },
};

/**
 * Renders Welcome [forename] [lastname]!
 *
 * @param  {Object} props
 * @return {React Element}
 */
const Welcome = (props) => {
    const { thisUser } = props;
    const { forename, surname } = thisUser;

    return (
        <div className="welcome">
            Welcome
            {` ${forename} ${surname}!`}
        </div>
    );
};
Welcome.propTypes = {
    thisUser: PropTypes.shape({
        forename: PropTypes.string,
        surname: PropTypes.string,
    }),
};
Welcome.defaultProps = {
    thisUser: {
        forename: 'Unkown',
        surname: 'Person',
    },
};
const WelcomeConnected = connect(store => ({ thisUser: store.thisUser }))(Welcome);

const rootComponent = (
    <App reducers={reducers}>
        <WelcomeConnected />
    </App>
);

render(rootComponent, '#app');
