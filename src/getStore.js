// -- packages --
import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

// const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
// const store = createStoreWithMiddleware(rootReducer);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/**
 * Creates a redux store
 *
 * @param  {Object} reducers - reducer functions
 * @return {Object} - Redux store
 */
function getStore (reducers) {
    const store = createStore(
        combineReducers(reducers),
        composeEnhancers(applyMiddleware(thunk))
    );

    return store;
};

export default getStore;
