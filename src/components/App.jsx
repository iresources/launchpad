// -- packages --
import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
// -- helpers --
import getStore from '../getStore';

let store = null;

/**
 * Renders the root component for an application
 * that wraps <Provider>
 *
 * @param  {Object} props
 * @return {Element}
 */
const App = (props) => {
    const { reducers, children } = props;
    if (reducers && !store) {
        store = getStore(reducers);
    }

    const content = (
        <div className="app-root">
            {children}
        </div>
    );

    // wrap in <Provider> if there's a store
    if (store) {
        return (
            <Provider store={store}>
                {content}
            </Provider>
        );
    }

    return content;
};

App.propTypes = {
    reducers: PropTypes.objectOf(PropTypes.func),
};

// module.exports = App;
export default App;
