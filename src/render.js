// -- packages --
// import React from 'react';
import ReactDOM from 'react-dom';

/**
 * Renders a component to the DOM
 *
 * @param  {Element} component
 * @param  {String} querySelector
 * @return {Void}
 */
const render = (component, querySelector) => {
    ReactDOM.render(component, document.querySelector(querySelector));
};

export default render;
