// -- components --
import App from './components/App';
// -- helpers --
import render from './render';
import getStore from './getStore';

module.exports = {
    components: {
        App,
    },
    helpers: {
        render,
        getStore,
    },
};
