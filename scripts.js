#!/usr/bin/env node

const { spawn } = require('child_process');
const fs = require('fs');
const path = require('path');
const drakov = require('drakov');
const Confirm = require('prompt-confirm');
// const jsdiff = require('diff');
// const runPromisesSequentially = require('./helpers/runPromisesSequentially');

/**
 * Runs a command in a child process and pipes output to console
 *
 * @param  {String} command
 * @return {Void}
 */
function runCommand(command = '', args) {
    const child = spawn(command, args, { stdio: 'inherit', sterr: 'inherit' });
    child.on('close', () => process.exit);
}

// function copyFile(source, target, cb) {
//   var cbCalled = false;

//   var rd = fs.createReadStream(source);
//   rd.on("error", function(err) {
//     done(err);
//   });
//   var wr = fs.createWriteStream(target);
//   wr.on("error", function(err) {
//     done(err);
//   });
//   wr.on("close", function(ex) {
//     done();
//   });
//   rd.pipe(wr);

//   function done(err) {
//     if (!cbCalled) {
//       cb(err);
//       cbCalled = true;
//     }
//   }
// }

/**
 * Makes a copy of a file in one location to another
 *
 * @param  {String} fromLocation
 * @param  {String} toLocation
 * @return {Promise}
 */
function copyFile(fromLocation, toLocation) {
    return new Promise((resolve, reject) => {
        if (!fromLocation || !toLocation) {
            return reject('Missing fromLocation or toLocation');
        }

        const readStream = fs.createReadStream(fromLocation);
        readStream.on('error', reject);

        const writeStream = fs.createWriteStream(toLocation);
        writeStream.on('error', reject);
        writeStream.on('close', resolve);

        readStream.pipe(writeStream);
    });
}

/**
 * Symlinks a file
 *
 * @param  {String} fromLocation
 * @param  {String} toLocation
 * @return {Promise}
 */
function symlinkFile(fromLocation, toLocation) {
    return new Promise((resolve, reject) => {
        fs.symlink(fromLocation, toLocation, 'file', (error) => {
            if (error) {
                return reject(error);
            }

            return resolve();
        });
    });
}

/**
 * Checks whether a file exists already or not
 *
 * @param  {String} fileLocation
 * @return {Bool}
 */
function fileExists(fileLocation) {
    const exists = fs.existsSync(fileLocation);
    return exists;
}

/**
 * Determines whether a file can be replaced
 *
 * @param  {String} fromLocation
 * @param  {String} toLocation
 * @return {Promise} -
 *   .then({Bool})
 */
function canPutFile(fromLocation, toLocation) {
    return new Promise((resolve) => {
        if (fileExists(toLocation)) {
            const prompt = new Confirm({
                name: `Replace ${toLocation}?`,
                message: 'Do you want to overwrite this file?',
            });

            prompt.ask(answer => resolve(answer));
        } else {
            return resolve(true);
        }
    });
}

// All the commands!
const actions = {
    /**
     * Runs hot reloading server via parceljs
     *
     * @return {Void}
     */
    start() {
        runCommand(
            'parcel',
            [
                'serve',
                'index.html',
                '--port=8080',
                // '--no-hmr'
            ],
        );
    },

    /**
     * Compiles files when they change via parceljs
     *
     * @return {Void}
     */
    watch() {
        runCommand(
            'parcel',
            [
                'watch',
                './src/index.js',
            ],
        );
    },

    /**
     * Copies template files to parent directory
     *
     * @return {Void}
     */
    copyTemplates() {
        const templates = [
            {
                filename: '.babelrc',
                destination: '.babelrc',
                method: 'copy',
            },
            {
                filename: 'index.html',
                destination: 'index.html',
                method: 'copy',
            },
            {
                filename: 'tsconfig.json',
                destination: 'tsconfig.json',
                method: 'copy',
            },
        ];

        // TODO:
        // Loop over each template and perform copy/symlink action
        // Make sure to ask for confirmation before writing over any existing files!
        Promise.all(
            templates.map((config) => {
                switch (config.method) {
                // case 'symlink':
                //     console.log('LINKING', config.filename);
                //     symlinkFile(`./templates/${config.filename}`, `../${config.destination}`);
                case 'copy':
                    canPutFile(config.filename, `../${config.destination}`)
                        .then((canPut) => {
                            if (canPut) {
                                return copyFile(`./templates/${config.filename}`, `../${config.destination}`);
                            }

                            return false;
                        });
                    break;
                default:
                    return Promise.resolve();
                }
            }),
        ).then(() => {
            console.log('Templates copied!');
        });
    },

    /**
     * Serves any .apib files in ../api_blueprints using Drakov
     *
     * @param {String} folder - folder containing .apib files. All files will
     * be served.
     * @return {Void}
     */
    startBlueprintServer(folder = './api_blueprints', watch = false, serverPort = 3001) {
        const config = {
            sourceFiles: path.join(__dirname, '../../', folder, '/**/*.apib'),
            serverPort,
            staticPaths: [],
            stealthmode: false,
            disableCors: false,
            autoOptions: true,
            delay: process.env.API_DEV_SERVER_DELAY || 1000,
            watch,
            public: true,
            method: [
                'get',
                'post',
                'delete',
                'options',
            ],
            header: [
                'Authorization',
            ],
        };

        drakov.run(config);
    },
};
/**
 * Runs the correct action
 *
 * @return {Void}
 */
function handleScript() {
    const args = process.argv;
    let scriptArgs = [...args];
    scriptArgs = scriptArgs.splice(2, scriptArgs.length);
    const action = scriptArgs[0];
    const actionArgs = scriptArgs.splice(1, scriptArgs.length);

    if (actions[action]) {
        console.log(`Running "${action}"`);
        actions[action](...actionArgs);
    } else {
        console.warn(`No "${action}" action available.`);
    }
}
handleScript();
