# Launchpad
Frontend projects more quickly using Typescript + React

## Getting Started
### Install Dependencies

`npm install -g parcel-bundler`

## API Blueprint Server
Serve `.apib` files as a development API using the Drakov NPM library.

1. Make a folder called `api_blueprints` in your project directory.
2. Drop your `.apib` files in here
3. Add to the "scripts" section of your `package.json` file: `"apiDevServer": "node ./node_modules/launchpad/scripts.js apiDevServer"`
4. Run `npm run apiDevServer`
5. Access API at `http://localhost:3001`
