/**
 * Runs an array of promises one by one, passing result of the previous into the next
 *
 * @param  {Array}  promises
 * @return {Promise}
 */
function runPromisesSequentially(promises = []) {
    return new Promise((resolve, reject) => {
        promises.reduce((promiseChain, currentTask) => {
            return promiseChain.then((chainResults) => {
                currentTask.then(currentResult => [...chainResults, currentResult])
                    .catch(reject);
            });
        }, resolve);
    });
}

module.exports = runPromisesSequentially;
